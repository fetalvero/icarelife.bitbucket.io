var ICL = ICL || {};

ICL.products = (function () {

    function init() {
        $('.product-models.electrocardiographs').show();
        initListeners();
    }

    function initListeners() {
        $('.list-group-item').on('click', function () {
            $('.list-group-item').removeClass('active');
            $(this).addClass('active');

            $('.product-models').hide();
            $('.product-models.' + $(this).data('type')).show();

        });
    }

    return {
        init: init

    };

}());
